package Score;


import java.util.Set;

public class Student implements Comparable<Student> {
	private String name;
	private String id;
	private int Score;

	public Student(String name, String id, int Score) {
		super();
		this.name = name;
		this.id = id;
		this.Score = Score;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getScore() {
		return Score;
	}

	public void setScore(int score) {
		Score = score;
	}

	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Student addScore(Student s) {
		this.Score += s.getScore();
		return this;
	}
	
	public Student getSameStudent(Set<Student> stuList) {
		for(Student s: stuList) {
			if(this.id.equals(s.getId())) {
				return s;
			}
		}
		return null;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Score;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (Score != other.Score)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return id + ", " + name + ", " +Score;
	}

	@Override
	public int compareTo(Student arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
}


