package Score;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Team {
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		//读取配置文件
		// 创建一个属性对象下载该文件
		Properties properties = new Properties();	
		properties.load(new FileInputStream("resources/config.properties"));
		
		//读取url和cookie
		String url = properties.getProperty("url");	
		String cookie = properties.getProperty("cookie");
		
		//创建两张哈希表用于存储学生和多个活动页面
        Set<Student> setStudentList = new HashSet<>();
        Set<String> setEventsList = new HashSet<>();

		try {	
			
			//用cookie创建一个jsoup链接
			Document html = Jsoup.connect(url).header("Cookie", cookie).get();
			Set<String> urls = getUrls(html);
			
			// 创建一个列表来保存所有的分数
			Map<String, Student> stuList = new TreeMap<>();
			
			Document create = Jsoup.connect(url).header("Cookie", cookie).get();
			//筛选每个学生的工作项目
				Elements activities = create.getElementsByClass("homework-item");
				Map<String, Student> oneActivity = getOneActivity(activities);
				oneActivity.forEach((key, value) -> {
					stuList.merge(key, value,
							(s1, s2) -> new Student(s1.getName(), s1.getId(), s1.getScore() + s2.getScore()));
				});

			// 在TreeSet中保存学生
			Set<Student> finalStuList = getStuInMap(stuList);
			String filename = "score.txt";
			writeTxtFile(finalStuList, filename);	// 将数据写入txt文件
			
	  } catch (IOException e) {
    	// TODO Auto-generated catch block
			e.printStackTrace();
	}
	}

	private static void creatNewfile(String filename) {
		
		File file = new File(filename);
		
		if (!file.exists()) {
			
			try {
				
				file.createNewFile();
				
			} catch (IOException e) {

				e.printStackTrace();
				
			}
		}
	}
	public static Set<Student> getStuInMap(Map<String, Student> stuList) {

		Set<Student> finalStuList = new TreeSet<>();

		// 使用forEach添加学生到finalStuList
		for (Student vl : stuList.values()) {

			finalStuList.add(vl);
		}
		return finalStuList;

	}

	private static Map<String, Student> getOneActivity(Elements activities) {

		Map<String, Student> s = new TreeMap<String, Student>();
		
		for (Element e : activities) {
			
			Student stu = getOneStudent(e);
			s.put(stu.getId(), stu);
			
		}

		return s;

	}
	public static Set<String> getUrls(Document html) {

		// 从源代码中获取所有的活动
		Elements activities = html.getElementsByClass("interaction-row");

		// 创建一个HashSet来保存类类型为"base"的url
		Set<String> urls = new HashSet<>();

		for (Element activity : activities) {

			// "课堂完成"
			if (activity.select("span[class~=^interaction-name]").text().contains("课堂完成")) {
				urls.add(activity.attr("data-url"));
			}

		}

		return urls;

	}

	public static Student getOneStudent(Element e) {

		Student stu = new Student();
		String[] msg = e.getElementsByClass("member-message").first().getElementsByTag("div").first().text().split(" ");
		stu.setName(msg[0]);
		stu.setId(msg[1]);
		Elements temp = e.select("div.appraised-type");
		if (temp.size() > 0) {
			Element finalScore = temp.get(1);
			if (finalScore.text().contains("未评分")) {
				stu.setScore(0);
			} else {
				stu.setScore(Integer.parseInt(finalScore.text().split(" ")[1]));
			}
		} else {
			stu.setScore(0);
		}

		return stu;

	}
	private static void writeTxtFile(Set<Student> finalStuList, String filename) {
		int i = 1;
		double mid = 0.0;
		int sum = 0;
		try {
			
			FileWriter fw = new FileWriter(filename);
			BufferedWriter bw = new BufferedWriter(fw);
			
			for (Student s : finalStuList) {

				if (i == 1) {
					bw.write("最高经验值：" + s.getScore() + "，");
				}

				sum += s.getScore();

				if (i == finalStuList.size()) {
					bw.write("最低经验值：" + s.getScore() + "，");
					mid = sum / finalStuList.size();
					bw.write("平均经验值：" + mid + "\n");
				}
				i++;

			}

			for (Student s : finalStuList) {

				bw.write(s.toString() + "\n");

			}
			
			bw.close();
			fw.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
